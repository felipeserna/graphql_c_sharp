# GraphQL in .NET

### GraphQL Backend
The GraphQL backend is created in C# using the [GraphQL NuGet Package](https://www.nuget.org/packages/GraphQL/).

| GraphiQL |
| -------- |
| ![GraphiQL](https://user-images.githubusercontent.com/13558917/67914743-3a8f4700-fb4e-11e9-936d-320dfdd5d874.png) |

## Getting Started

This app requires us to run the GraphQL API using the terminal while using Visual Studio to build/deploy the Xamarin.iOS and/or Xamarin.Android app.

### 1. Run the GraphQL API

1. Open the **terminal**
2. In the **terminal**, clone this solution by entering the following command:
    - **Note:** If you have already downloaded the solution, skip this step

```bash
git clone https://github.com/brminnick/dotnetgraphql.git
```

3. In the **terminal**, navigate to the `DotNetGraphQL.API` folder by entering the following command:

- On Windows

```bash
cd [path to DotNetGraphQL folder]\Source\DotNetGraphQL.API\
```
- On macOS
```bash
cd [path to DotNetGraphQL folder]/Source/DotNetGraphQL.API/
```

4. In the **terminal**, run `DotNetGraphQL.API.csproj` by entering the following command:

```bash
dotnet run
```

5. Open a web browser
6. In the web browser, navgiate to `http://localhost:4000`
7. Confirm GraphiQL
    
### GraphQL Resources

- [GraphQL.org](https://graphql.org/)
- [Using Postman with GraphQL API](https://www.codetraveler.io/2019/01/12/how-to-use-postman-with-a-graphql-api/)
- [GraphQL for .NET](https://github.com/graphql-dotnet/graphql-dotnet)
- [GraphQL vs REST](https://philsturgeon.uk/api/2017/01/24/graphql-vs-rest-overview/)
- [GraphQL vs OData](https://jeffhandley.com/2018-09-13/graphql-is-not-odata)
- [Awesome GraphQL](https://github.com/chentsulin/awesome-graphql)
